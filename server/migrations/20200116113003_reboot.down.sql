-- This file should undo anything in `up.sql`

DROP TABLE sessions;
DROP TABLE users;
DROP TABLE pending_users;
DROP TABLE deleted_usernames;
