module gitlab.com/bloom42/bloom/server

go 1.13

require (
	github.com/99designs/gqlgen v0.10.3-0.20200124025457-f7a67722a6ba
	github.com/agnivade/levenshtein v1.0.3 // indirect
	github.com/getsentry/sentry-go v0.4.0
	github.com/go-chi/chi v4.0.3+incompatible
	github.com/go-chi/cors v1.0.0
	github.com/golang-migrate/migrate/v4 v4.8.0
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/google/uuid v1.1.1
	github.com/gorilla/websocket v1.4.1 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.3.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/spf13/cobra v0.0.5
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/stripe/stripe-go v68.13.0+incompatible
	github.com/vektah/gqlparser v1.2.1
	gitlab.com/bloom42/bloom/common v0.0.0-20200131135300-ecb6e6c5a33d
	gitlab.com/bloom42/libs/crypto42-go v0.0.0-20200118201250-b035ee487899
	gitlab.com/bloom42/libs/rz-go v1.3.0
	gitlab.com/bloom42/libs/sane-go v0.10.0
	golang.org/x/sys v0.0.0-20200122134326-e047566fdf82 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
