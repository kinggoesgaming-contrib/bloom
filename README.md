<p align="center">
  <img alt="bloom logo" src="assets/icons/bloom_256.png" height="180" />
  <h3 align="center">Bloom</h3>
  <!-- <p align="center">Empowering people with open technologies 🌍 (Android, IOS, Linux, MacOS, Windows, Server, Website)</p> -->
</p>

You no longer trust tech monopolies?<br/>
You are done with your privacy invaded by advertisers? <br/>
You are tired of being abused by Big Companies?

**We too, so we built Bloom.**

[Try it for free](https://bloom.sh/download)

--------

Why? How? What? 👉 Read the launch post: https://fatalentropy.com/bloom-a-free-and-open-source-google

1. [Download](#download)
2. [Documentation](#documentation)
3. [Development](#development)
4. [Contributing](#contributing)
5. [Forum](#contributing)
6. [Licensing](#licensing)
7. [Security](#security)

--------

## Download

Android: [Google play store](https://play.google.com/store/apps/details?id=com.bloom42.bloomx) <br/>
iOS: Coming soon


Linux: Coming soom <br/>
MacOS: Coming soon <br/>
Windows: Coming soon


## Documentation

See [the wiki](https://gitlab.com/bloom42/bloom/-/wikis)


## Development

See https://gitlab.com/bloom42/bloom/-/wikis/development


## Contributing

Thank you for your interest in contributing! Please refer to
https://gitlab.com/bloom42/wiki/-/wikis/organization/contributing for guidance.


## Forum

When you want to talk about something that does not necessarily fit issues, you can use the forum
to connect with the community: https://gitlab.com/bloom42/forum

## Licensing

See `LICENSE.txt` and https://gitlab.com/bloom42/wiki/-/wikis/organization/licensing


## Security

If you found a security issue affecting this project, please do not open a public issue and refer to our
[dedicated security page](https://bloom.sh/security) instead. Thank you!
