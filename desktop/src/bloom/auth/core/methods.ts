enum Method {
  StartRegistration = 'users.start_registration',
  VerifyRegistration = 'users.verify_registration',
  CompleteRegistration = 'users.complete_registration',
  SignIn = 'users.sign_in',
  SignOut = 'users.sign_out',
}

export default Method;
