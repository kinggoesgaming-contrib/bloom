/* eslint-disable camelcase */

export type Expression = {
  expression: string,
}

export type Result = {
  result: string,
}
