enum Method {
  ListNotes = 'notes.list_notes',
  ListArchived = 'notes.list_archived',
  CreateNote = 'notes.create_note',
  UpdateNote = 'notes.update_note',
  DeleteNote = 'notes.delete_note',
}

export default Method;
