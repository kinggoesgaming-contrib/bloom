enum Method {
  ListEvents = 'calendar.list_events',
  CreateEvent = 'calendar.create_event',
  UpdateEvent = 'calendar.update_event',
  DeleteEvent = 'calendar.delete_event',
}

export default Method;
