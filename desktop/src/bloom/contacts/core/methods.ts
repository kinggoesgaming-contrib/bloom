enum Method {
  ListContacts = 'contacts.list_contacts',
  CreateContact = 'contacts.create_contact',
  UpdateContact = 'contacts.update_contact',
  DeleteContact = 'contacts.delete_contact',
}

export default Method;
