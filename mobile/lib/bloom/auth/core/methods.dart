class AuthMethod {
  static const String start_registration = 'accounts.start_registration';
  static const String verify_registration = 'accounts.verify_registration';
  static const String complete_registration = 'accounts.complete_registration';
  static const String sign_in = 'accounts.sign_in';
  static const String sign_out = 'accounts.sign_out';
}
