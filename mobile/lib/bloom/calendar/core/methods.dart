class CalendarMethod {
  static const String list_events = 'calendar.list_events';
  static const String create_event = 'calendar.create_event';
  static const String update_event = 'calendar.update_event';
  static const String delete_event = 'calendar.delete_event';
}
