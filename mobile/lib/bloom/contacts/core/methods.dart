class ContactsMethod {
  static const String list_contacts = 'contacts.list_contacts';
  static const String create_contact = 'contacts.create_contact';
  static const String update_contact = 'contacts.update_contact';
  static const String delete_contact = 'contacts.delete_contact';
}
