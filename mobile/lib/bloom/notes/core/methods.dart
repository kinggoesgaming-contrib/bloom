class NotesMethod {
  static const String list_notes = 'notes.list_notes';
  static const String list_archived = 'notes.list_archived';
  static const String delete_note = 'notes.delete_note';
  static const String update_note = 'notes.update_note';
  static const String create_note = 'notes.create_note';
}
