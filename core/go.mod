module gitlab.com/bloom42/bloom/core

go 1.13

require (
	github.com/golang/protobuf v1.3.2
	github.com/google/uuid v1.1.1
	github.com/mattn/go-sqlite3 v2.0.2+incompatible
	github.com/twitchtv/twirp v5.10.1+incompatible // indirect
	gitlab.com/bloom42/bloom/common v0.0.0-20200127090540-a11887f64516
	golang.org/x/crypto v0.0.0-20200117160349-530e935923ad
	golang.org/x/sys v0.0.0-20200122134326-e047566fdf82 // indirect
)
